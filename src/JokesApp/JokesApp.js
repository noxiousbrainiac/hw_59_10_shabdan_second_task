import React, {useEffect, useState} from 'react';
import '../assets/bootstrap.min.css';

const JokesApp = () => {
    const [data, setData] = useState([]);

    useEffect(() => {
        fetch(`https://api.chucknorris.io/jokes/random`)
            .then(response => response.json())
            .then(json => setData(json))
    }, []);

    const joke = JSON.stringify(data.value, null, 2);

    return (
        <div className="container">
            <div className="card m-5 border-dark">
                <p className="text-center p-4 m-0">{joke}</p>
            </div>
        </div>
    );
};

export default JokesApp;