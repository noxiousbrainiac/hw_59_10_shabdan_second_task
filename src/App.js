import React from 'react';
import JokesApp from "./JokesApp/JokesApp";

const App = () => {
    return (
        <div>
          <JokesApp/>
        </div>
    );
};

export default App;